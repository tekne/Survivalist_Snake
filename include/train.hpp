#ifndef TRAIN_SNAKENN_INCLUDED
#define TRAIN_SNAKENN_INCLUDED

#include<algorithm>
#include <snakenn.hpp>

namespace ssnake
{
  double score_snake(SnakeNN&, GameBoard&);
  std::vector<SnakeNN>& score_generation(
    std::vector<SnakeNN>&, std::vector<GameBoard>&, size_t, long nt = 1
  );

  inline std::vector<SnakeNN>& stratify_generation(
    std::vector<SnakeNN>& v,
    size_t n
  )
  {
    if(n >= v.size()) return v;
    std::nth_element(
      v.begin(),
      v.begin() + n,
      v.end(),
      [](const SnakeNN& s1, const SnakeNN& s2) {return s1.score() > s2.score();}
    );
    return v;
  }

  template<class D, class G>
  inline std::vector<SnakeNN>& clone_generation(
    std::vector<SnakeNN>& v, size_t n, size_t c, D d, G g
  )
  {
    assert(n <= v.size());
    assert(n > 0);
    assert(c > 0);
    v.resize(n);
    v.reserve(c * n);
    for(size_t i = 1; i < c; i++)
    {
      for(size_t j = 0; j < n; j++)
      {
        v.push_back(v[j].mutant(d, g));
      }
    }
    return v;
  }

  inline std::array<double, 8> score_statistics(const std::vector<SnakeNN>& v)
  {
    double ts = 0;
    double tmas = 0;
    double tmis = 0;
    double max_s = 0;
    double min_s = 0;
    for(const auto& s: v)
    {
      ts += s.score();
      //printf("SUMND: %lf, %lf, %lf\n", s.score(), s.max_score(), s.min_score());
      tmas += s.max_score();
      tmis += s.min_score();
      if((!min_s) || (s.score() < min_s)) min_s = s.score();
      if(s.score() > max_s) max_s = s.score();
    }
    ts /= v.size();
    //printf("TOTAL: %lf, %lf, %lf\n", ts, tmas, tmis);
    tmas /= v.size();
    tmis /= v.size();
    double var = 0;
    double maxvar = 0;
    double minvar = 0;
    for(const auto& s: v)
    {
       var += (s.score() - ts) * (s.score() - ts);
       maxvar += (s.max_score() - tmas) * (s.max_score() - tmas);
       minvar += (s.max_score() - tmis) * (s.min_score() - tmis);
    }
    var /= v.size();
    maxvar /= v.size();
    minvar /= v.size();
    return {ts, sqrt(var), tmas, sqrt(maxvar), tmis, sqrt(minvar), max_s, min_s};
  }
}

#endif
