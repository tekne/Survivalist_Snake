#ifndef GAME_INCLUDED
#define GAME_INCLUDED

#include <cmath>
#include <cstring>
#include <cassert>
#include <cstdlib>
#include <vector>
#include <array>
#include <algorithm>

namespace ssnake
{

  typedef long pos_t;
  typedef unsigned long time_t;

  enum Direction {
    DIR_N = 0,
    DIR_S = 1,
    DIR_W = 2,
    DIR_E = 3,
    DIR_NE = 4,
    DIR_NW = 5,
    DIR_SE = 6,
    DIR_SW = 7
  };

  struct BoardPosition
  {
    pos_t x;
    pos_t y;

    BoardPosition operator+(BoardPosition p) const {return {x + p.x, y + p.y};}
    BoardPosition operator-(BoardPosition p) const {return {x + p.x, y + p.y};}
    BoardPosition operator*(pos_t c) const {return {x*c, y*c};}
    BoardPosition operator-() const {return {-x, -y};}
    BoardPosition& operator+= (BoardPosition p) {
      x += p.x; y += p.y;
      return *this;
    }
    BoardPosition& operator-= (BoardPosition p) {
      x -= p.x; y -= p.y;
      return *this;
    }
    BoardPosition& operator*= (pos_t c) {
      x *= c; y *= c;
      return *this;
    }
    pos_t operator*(BoardPosition p) const {return x * p.x + y * p.y;}
    double dist2(BoardPosition p) const {return (*this - p) * (*this - p);}
    double dist(BoardPosition p) const {return hypot(x - p.x, y - p.y);}
    double norm2() const {return *this * *this;}
    double norm() const {return hypot(x, y);}
    bool operator==(BoardPosition p) const {return (x == p.x) && (y == p.y);}

    bool in_direction(Direction d)
    {
      switch(d)
      {
        case DIR_N: return !x && (y >= 0);
        case DIR_S: return !x && (y <= 0);
        case DIR_E: return !y && (x >= 0);
        case DIR_W: return !y && (x <= 0);
        case DIR_NE: return (x >= 0) && (x == y);
        case DIR_NW: return (x <= 0) && (x == -y);
        case DIR_SE: return (x >= 0) && (x == -y);
        case DIR_SW: return (x <= 0) && (x == y);
        default: return false;
      }
    }

    pos_t& operator[](unsigned index)
    {
      assert(index < 2);
      return index?y:x;
    }

    const pos_t& operator[](unsigned index) const
    {
      assert(index < 2);
      return index?y:x;
    }
  };

  inline BoardPosition operator*(pos_t c, BoardPosition p) {return p * c;}

  static inline BoardPosition invalid_pos() {return {-1, -1};}
  static inline BoardPosition move_from_dir(Direction d)
  {
    switch(d)
    {
      case DIR_N:
        return {0, 1};
      case DIR_S:
        return {0, -1};
      case DIR_W:
        return {-1, 0};
      case DIR_E:
        return {1, 0};
      case DIR_NE:
        return {1, 1};
      case DIR_NW:
        return {-1, 1};
      case DIR_SE:
        return {1, -1};
      case DIR_SW:
        return {-1, -1};
      default:
        return {0, 0};
    }
  }

  class GameBoard
  {
    BoardPosition fp;
    std::vector<BoardPosition> snake;
    pos_t boardX;
    pos_t boardY;
    time_t t;
    time_t pm;
    time_t mh;
  public:
    GameBoard(pos_t X, pos_t Y, time_t MH = 1000): boardX(X), boardY(Y),
    t(0), pm(0), mh(MH), fp(invalid_pos()), snake() {}


    bool in_bounds(BoardPosition p) const {
      return (p.x >= 0) && (p.y >= 0) && (p.x < boardX) && (p.y < boardY);
    }

    bool snake_valid() const {
      for(const auto& s: snake) if(!in_bounds(s)) return false;
      return true;
    }

    pos_t width() const {return boardX;}
    pos_t height() const {return boardY;}
    auto size() const {return snake.size();}
    time_t turns() const {return t;}
    time_t prev_meal() const {return pm;}
    time_t max_hunger() const {return mh;}
    bool has_snake() const {return !snake.empty() && snake_valid();}
    bool has_food() const {return in_bounds(fp);}
    BoardPosition food_pos() const {return fp;}
    BoardPosition head_pos() const
    {
      if(snake.empty()) return invalid_pos();
      return snake.back();
    }

    bool is_food(BoardPosition p) const {return fp == p;}
    bool is_head(BoardPosition p) const {return head_pos() == p;}
    bool is_snake(BoardPosition p) const {
      for(const auto& s: snake) if(s == p) return true;
      return false;
    }
    bool is_snake_nh(BoardPosition p) const {
      if(!snake.size()) return false;
      for(size_t i = 0; i < snake.size() - 1; i++)
      {
        if(p == snake[i]) return true;
      }
      return false;
    }

    double hunger() const {
      return static_cast<double>(prev_meal())/(max_hunger() * size());
    }

    double score() const {
      return 0.0001 * (1 - (2*hunger() - 1) * (2*hunger() - 1)) * turns()
      + size() - 1;
    }

    bool is_over() const {
      return snake.empty() || !in_bounds(head_pos()) || is_snake_nh(head_pos())
      || (prev_meal() > max_hunger() * size());
    }

    double distance_to_border(Direction d, BoardPosition pos) const {
      if(!has_snake()) return 0;
      switch(d)
      {
      case DIR_N:
        return boardY - 1 - pos.y;
      case DIR_E:
        return boardX - 1 - pos.x;
      case DIR_W:
        return pos.x;
      case DIR_S:
        return pos.y;
      case DIR_NE:
        return std::min(
          distance_to_border(DIR_N), distance_to_border(DIR_E)
        );
      case DIR_NW:
        return std::min(
          distance_to_border(DIR_N), distance_to_border(DIR_W)
        );
      case DIR_SE:
        return std::min(
          distance_to_border(DIR_S), distance_to_border(DIR_E)
        );
      case DIR_SW:
        return std::min(
          distance_to_border(DIR_S), distance_to_border(DIR_W)
        );
      }
      return 0;
    }
    double distance_to_border(Direction d) const {
      return distance_to_border(d, head_pos());
    }
    std::array<double, 8> distance_to_border(BoardPosition pos) const {
      std::array<double, 8> result;
      for(unsigned i = 0; i < 8; i++) result[i] = distance_to_border(
        static_cast<Direction>(i), pos
      );
      return result;
    }
    std::array<double, 8> distance_to_border() const {
      return distance_to_border(head_pos());
    }

    double distance_to_obstacle(Direction d, BoardPosition opos) const
    {
      assert(!snake.empty());
      using std::abs;
      BoardPosition pos = invalid_pos();
      for(size_t i = 0; i < snake.size() - 1; i++)
      {
        if((snake[i] - opos).in_direction(d))
        {
          if(!in_bounds(pos)
            || ((snake[i] - opos) * move_from_dir(d)
            < (pos - opos) * move_from_dir(d))
          ) pos = snake[i];
        }
      }
      if(!in_bounds(pos)) return distance_to_border(d);
      return std::max(abs((pos - opos).x), abs((pos - opos).y));
    };
    double distance_to_obstacle(Direction d) const
    {
      return distance_to_obstacle(d, head_pos());
    }
    std::array<double, 8> distance_to_obstacle(BoardPosition pos) const
    {
        std::array<double, 8> result;
        for(unsigned i = 0; i < 8; i++) result[i] = distance_to_obstacle(
          static_cast<Direction>(i), pos
        );
        return result;
    }
    std::array<double, 8> distance_to_obstacle() const {
      return distance_to_obstacle(head_pos());
    }
    BoardPosition distance_to_food(BoardPosition pos) const {
      return pos - fp;
    }
    BoardPosition distance_to_food() const {
      return head_pos() - fp;
    }

    double nearest_obstacle(BoardPosition opos) const {
      BoardPosition pos = invalid_pos();
      double dist = 0;
      for(size_t i = 0; i < snake.size() - 1; i++)
      {
        if((snake[i] - opos).norm() > dist)
        {
          dist = (snake[i] - opos).norm();
        }
      }
      if(!in_bounds(pos)) return std::min(
        {distance_to_border(static_cast<Direction>(0)),
         distance_to_border(static_cast<Direction>(1)),
         distance_to_border(static_cast<Direction>(2)),
         distance_to_border(static_cast<Direction>(3))}
      );
      return std::min(
        {dist,
         distance_to_border(static_cast<Direction>(0)),
         distance_to_border(static_cast<Direction>(1)),
         distance_to_border(static_cast<Direction>(2)),
         distance_to_border(static_cast<Direction>(3))}
      );
    }
    double nearest_obstacle() const {return nearest_obstacle(head_pos());}

    BoardPosition random_in_bounds() const {
      return {rand() % boardX, rand() % boardY};
    }

    GameBoard& delete_snake() {
      snake.clear();
      return *this;
    }
    GameBoard& new_snake(BoardPosition p) {
      assert(in_bounds(p));
      snake = {p};
      return *this;
    }
    GameBoard& new_snake() {
      return new_snake(random_in_bounds());
    }
    GameBoard& delete_food() {
      fp = invalid_pos();
      return *this;
    }
    GameBoard& new_food(BoardPosition p) {
      assert(in_bounds(p));
      fp = p;
      return *this;
    }
    GameBoard& new_food() {
      return new_food(random_in_bounds());
    }
    GameBoard& clear_board() {
      delete_snake();
      delete_food();
      t = 0;
      pm = 0;
      return *this;
    }
    GameBoard& forward_snake(Direction d) {
      assert(!snake.empty());
      BoardPosition new_head = snake.back() + move_from_dir(d);
      memmove(
        snake.data(),
        snake.data() + 1,
        sizeof(BoardPosition) * (snake.size() - 1)
      );
      snake.back() = new_head;
      return *this;
    }
    GameBoard& grow_snake(Direction d) {
      assert(!snake.empty());
      snake.push_back(snake.back() + move_from_dir(d));
      return *this;
    }
    GameBoard& move_snake(Direction d) {
      assert(!snake.empty());
      if(is_food(snake.back() + move_from_dir(d)))
      {
        grow_snake(d);
        new_food();
        pm = 0;
      }
      else
      {
        forward_snake(d);
        pm++;
      }
      t++;
      return *this;
    }

  };

}

#endif
