#ifndef GAME_INTERFACE_INCLUDED
#define GAME_INTERFACE_INCLUDED

#include <game.hpp>
#include <chrono>
#include <thread>

namespace ssnake
{

  const GameBoard& printBoard(const GameBoard& g);
  void initializeScreen();
  void deleteScreen();
  void endWindow();
  void driveBoard(GameBoard& g, char key);
  void userPlay(GameBoard& g);

  template<class N, class R, class P>
  void showAI(GameBoard& g, N& agent, std::chrono::duration<R, P> t)
  {
    if(!g.is_over())
    {
      do {
        printBoard(g);
        g.move_snake(agent.move(g));
        std::this_thread::sleep_for(t);
      } while(!g.is_over());
      endWindow();
    }
  }

}

#endif
