#ifndef SNAKE_MANAGER_INCLUDED
#define SNAKE_MANAGER_INCLUDED

#include <vector>

#include <snakenn.hpp>

#include<istream>
#include<ostream>

namespace ssnake {

  template<class T>
  std::ostream& store_datavector(std::ostream& stream, const std::vector<T>& v)
  {
    size_t size = v.size();
    stream.write(reinterpret_cast<const char*>(&size), sizeof size);
    stream.write(reinterpret_cast<const char*>(v.data()), size * sizeof(T));
    return stream;
  }

  template<class T>
  std::vector<T> read_datavector(std::istream& stream)
  {
    size_t size;
    stream.read(reinterpret_cast<char*>(&size), sizeof size);

    std::vector<T> result(size);
    stream.read(reinterpret_cast<char*>(result.data()), size * sizeof(T));
    return result;
  }

  SnakeNN load_old_snakefile(std::istream&);
  SnakeNN load_snake(std::istream&);
  std::vector<SnakeNN> load_snakefile(std::istream&);

  std::ostream& store_snake(std::ostream&, const SnakeNN&);
  std::ostream& store_snakefile(std::ostream&, const std::vector<SnakeNN>&);

}

#endif
