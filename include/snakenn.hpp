#ifndef SNAKENN_INCLUDED
#define SNAKENN_INCLUDED

#include <cstddef>
#include <cstdlib>
#include <cstdio>
#include <cassert>
#include <vector>
#include <initializer_list>
#include <array>
#include <utility>

#include <game.hpp>

namespace ssnake {

  const int IV_SIZE = 64;
  const int OV_SIZE = 4;

  size_t hyper_size(const unsigned* hyper, unsigned c);
  static inline size_t hyper_size(const std::vector<unsigned>& hv)
  {
    return hyper_size(hv.data(), hv.size());
  }
  static inline std::array<Direction, 5> init_memory() {return {DIR_NE, DIR_NE, DIR_NE, DIR_NE, DIR_NE};}

  class SnakeNN
  {
  public:
    std::vector<unsigned> hpv;
    std::vector<double> weights;
    unsigned gen;
    double sc;
    double max_sc;
    double min_sc;
    std::array<Direction, 5> memory;

    SnakeNN(): hpv(), weights(hyper_size(NULL, 0)), gen(0), sc(0), memory(init_memory()) {}
    SnakeNN(std::vector<unsigned> v, double s = 0, unsigned g = 0): hpv(v), weights(hyper_size(v)), gen(g), sc(s),
    memory(init_memory()) {}
    SnakeNN(std::vector<unsigned> v, std::vector<double> w, double s = 0, unsigned g = 0):
    hpv(v), weights(w), gen(g), sc(s), memory(init_memory())
    {
      assert(w.size() >= hyper_size(hpv));
    }

    SnakeNN& operator=(const SnakeNN& s)
    {
      hpv = s.hpv; weights = s.weights; gen = s.gen;
      sc = s.sc; memory = s.memory;
      return *this;
    }
    SnakeNN& operator=(SnakeNN&& s)
    {
      hpv = std::move(s.hpv); weights = std::move(s.weights);
      gen = s.gen; sc = s.sc; memory = s.memory;
      return *this;
    }

    SnakeNN(const SnakeNN& s): hpv(s.hpv), weights(s.weights), gen(s.gen), sc(s.sc), memory(s.memory) {}
    SnakeNN(SnakeNN&& s): hpv(std::move(s.hpv)), weights(std::move(s.weights)), gen(s.gen), sc(s.sc), memory(s.memory) {}

    std::array<double, OV_SIZE> propagate(std::array<double, IV_SIZE>) const;
    std::array<double, OV_SIZE> propagate(const GameBoard&) const;
    Direction move(const GameBoard&);

    template<class D, class G> SnakeNN& mutate(D d, G g)
    {
      size_t hc = hyper_size(hpv);
      for(size_t i = 0; i < hc; i++) weights[i] += d(g);
      gen++;
      return *this;
    }
    template<class D, class G> SnakeNN mutant(D d, G g)
    {
      SnakeNN s = *this;
      s.mutate(d, g);
      return s;
    }

    size_t size() const {return hyper_size(hpv);}

    void print_snake(FILE* f = stdout)
    {
      fprintf(f, "HPC: %zu, HYP: %zu, TRUE: %zu, SC: %lf, GN: %u\n",
        hpv.size(), hyper_size(hpv), weights.size(), sc, gen
      );
    }

    double score() const {return sc;}
    double max_score() const {return max_sc;}
    double min_score() const {return min_sc;}
    SnakeNN& set_score(double s) {sc = s; return *this;}
    SnakeNN& set_max_score(double s) {max_sc = s; return *this;}
    SnakeNN& set_min_score(double s) {min_sc = s; return *this;}
    SnakeNN& set_gen(unsigned g) {gen = g; return *this;}

  };

}

#endif
