#include <game.hpp>
#include <game_interface.hpp>
#include <snakenn.hpp>
#include <manager.hpp>
#include <cstdio>
#include <random>
#include <fstream>
#include <cstdlib>
#include <ctime>
#include <cstdlib>

int main(int argc, char** argv)
{

  srand(time(NULL));

  ssnake::initializeScreen();
  long x, y;
  if(argc == 2)
  {
    fprintf(stderr, "Invalid number of arguments (1)!\n");
    return 1;
  }
  else if(argc > 6)
  {
    fprintf(stderr, "Too many arguments (%i)\n", argc - 1);
    return 1;
  }
  if(argc == 1)
  {
    printf("Input board dimensions (X, Y): ");
    scanf(" ( %li , %li )", &x, &y);
  }
  else
  {
    x = strtol(argv[1], NULL, 0);
    if(x <= 0)
    {
      fprintf(stderr, "Invalid board width %s\n", argv[1]);
      return 1;
    }
    y = strtol(argv[2], NULL, 0);
    if(y <= 0)
    {
      fprintf(stderr, "Invalid board height %s\n", argv[2]);
      return 1;
    }
  }
  ssnake::GameBoard g = ssnake::GameBoard(x, y);
  g.new_snake();
  g.new_food();
  if(argc > 3)
  {
    long delay;
    long snake;
    std::fstream file(argv[3]);
    printf("Loading...\n");
    std::vector<ssnake::SnakeNN> snakes = ssnake::load_snakefile(file);
    printf("Loaded %zu snakes...\n", snakes.size());

    if(argc > 4)
    {
      snake = strtol(argv[4], NULL, 0);
      if(snake < 0)
      {
        fprintf(stderr, "Invalid index %s\n", argv[4]);
        return 1;
      }
    }
    else
    {
      printf("Select snake: ");
      scanf(" %ld", &snake);
      if(snake < 0)
      {
        fprintf(stderr, "Invalid index!\n");
        return 1;
      }
    }
    if(snake >= snakes.size())
    {
      fprintf(stderr, "Index %ld out of bounds!\n", snake);
      return 1;
    }
    if(argc > 5)
    {
      delay = strtol(argv[5], NULL, 0);
      if(delay <= 0)
      {
        fprintf(stderr, "Invalid delay %s\n", argv[5]);
        return 1;
      }
    }
    else
    {
      printf("Input delay (ms): \n");
      scanf(" %ld", &delay);
      if(delay <= 0)
      {
        fprintf(stderr, "Invalid delay!\n");
        return 1;
      }
    }
    ssnake::showAI(g, snakes[snake], std::chrono::milliseconds(delay));
  }
  else
  {
    ssnake::userPlay(g);
  }
  printf(
    "Game Over! Length: %li, Turns: %zu, Score: %lf\n",
    g.size(), g.turns(), g.score()
  );
  ssnake::deleteScreen();
  return 0;
}
