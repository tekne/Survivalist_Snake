#include <manager.hpp>
#include <fstream>

int main(int argc, char** argv)
{
  if(!(argc % 2))
  {
    fprintf(
      stderr,
      "Invalid number (%d) of arguments. Must be a nonzero multiple of 2\n",
      argc - 1
    );
  }
  std::fstream ifile;
  std::fstream ofile;
  for(int i = 1; i < argc; i += 2)
  {
    ifile.open(argv[i]);
    ofile.open(argv[i + 1], std::fstream::out);
    fprintf(stderr, "Loading snake %i from %s...\n", (i + 1)/2, argv[i]);
    ssnake::SnakeNN s = ssnake::load_old_snakefile(ifile);
    std::vector<ssnake::SnakeNN> sv = {s};
    fprintf(stderr, "Storing snake %i to %s...\n", (i + 1)/2, argv[i + 1]);
    ssnake::store_snakefile(ofile, sv);
  }
}
