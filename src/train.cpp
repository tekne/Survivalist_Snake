#include <game.hpp>
#include <snakenn.hpp>
#include <train.hpp>

#include <thread>
#include <functional>

namespace ssnake
{

  double score_snake(SnakeNN& s, GameBoard& g) {
    //s.print_snake();
    g.new_snake();
    g.new_food();
    while(!g.is_over())
    {
      g.move_snake(s.move(g));
    }
    s.set_score(g.score());
    g.clear_board();
    return s.score();
  }

  template<class IT>
  void score_ensemble(IT a, IT b, std::vector<GameBoard>& g, size_t r)
  {
    double tsc, sc, min_sc, max_sc;
    size_t gc = g.size();
    while(a != b)
    {
      sc = 0;
      max_sc = 0;
      min_sc = 0;
      for(auto& b: g) for(size_t i = 0; i < r; i++) {
        tsc = score_snake(*a, b);
        sc += tsc;
        if(tsc > max_sc) max_sc = tsc;
        if((!min_sc) || (tsc < min_sc)) min_sc = tsc;
      }
      //printf("S: %lf, R: %zu, G: %zu, A: %lf (MIN: %lf, MAX: %lf)\n", sc, r, gc, sc/(r * gc), min_sc, max_sc);
      a->set_score(sc/(r * gc));
      a->set_max_score(max_sc);
      a->set_min_score(min_sc);
      //printf("BECOMES: A: %lf, MIN: %lf, MAX: %lf\n", a->score(), a->min_sc, a->max_sc);
      a++;
    }
  }

  std::vector<SnakeNN>& score_generation(
    std::vector<SnakeNN>& v,
    std::vector<GameBoard>& g,
    size_t r,
    long num_threads
  )
  {
    if(num_threads <= 1)
    {
      score_ensemble(v.begin(), v.end(), g, r);
      return v;
    }
    size_t chunk = v.size() / num_threads;

    std::vector<std::vector<GameBoard>> gv (num_threads, g);

    std::vector<std::thread> threads;
    threads.reserve(num_threads);
    auto b = v.begin();
    for(unsigned i = 0; i < num_threads - 1; i++)
    {
      threads.emplace_back(
        score_ensemble<decltype(b)>,
        b + i * chunk,
        b + (i + 1) * chunk,
        std::ref(gv[i]),
        r
      );
    }
    threads.emplace_back(
      score_ensemble<decltype(b)>,
      b + (num_threads - 1) * chunk,
      v.end(),
      std::ref(gv.back()), r
    );
    for(auto& t : threads) t.join();
    return v;
  }




}
