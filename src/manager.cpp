#include <manager.hpp>
#include <game.hpp>
#include <snakenn.hpp>

#include <cstdio>
#include <stdexcept>

#include <ostream>
#include <istream>

namespace ssnake {

  const unsigned long magic_number = 0x8A534E414B45464C;

  SnakeNN load_old_snakefile(std::istream& stream)
  {
    std::vector<unsigned> hpv = {32};
    std::vector<double> weights (hyper_size(hpv));
    char* w = reinterpret_cast<char*>(weights.data());
    for(unsigned i = 0; i < 32; i++)
    {
      stream.read(w, 41 * sizeof(double));
      w += OV_SIZE * sizeof(double);
    }
    stream.read(w, (32 + 32*4) * sizeof(double));
    w += 32 * IV_SIZE * sizeof(double);
    stream.read(w, 4 * sizeof(double));
    return SnakeNN{hpv, weights};
  }

  SnakeNN load_snake(std::istream& stream)
  {
    auto hpv = read_datavector<unsigned>(stream);
    auto weights = read_datavector<double>(stream);
    unsigned gen; double sc;
    stream.read(reinterpret_cast<char*>(&gen), sizeof gen);
    stream.read(reinterpret_cast<char*>(&sc), sizeof sc);
    return SnakeNN{hpv, weights, sc, gen};
  }

  std::vector<SnakeNN> load_snakefile(std::istream& stream)
  {
    unsigned long mn;
    unsigned size;
    stream.read(reinterpret_cast<char*>(&mn), sizeof mn);
    if(mn != magic_number) throw std::runtime_error("Invalid magic number!");
    stream.read(reinterpret_cast<char*>(&size), sizeof size);

    std::vector<SnakeNN> result;
    result.reserve(size);
    for(unsigned i = 0; i < size; i++)
    {
      result.push_back(load_snake(stream));
    }
    return result;
  }

  std::ostream& store_snake(std::ostream& stream, const SnakeNN& s)
  {
    store_datavector(stream, s.hpv);
    store_datavector(stream, s.weights);
    stream.write(reinterpret_cast<const char*>(&s.gen), sizeof s.gen);
    stream.write(reinterpret_cast<const char*>(&s.sc), sizeof s.sc);
    return stream;
  }

  std::ostream& store_snakefile(
    std::ostream& stream,
    const std::vector<SnakeNN>& v
  )
  {
    stream.write(reinterpret_cast<const char*>(&magic_number), sizeof magic_number);
    unsigned size = v.size();
    stream.write(reinterpret_cast<const char*>(&size), sizeof size);
    for(const auto& s : v) store_snake(stream, s);
    return stream;
  }


}
