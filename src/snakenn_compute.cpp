#include <snakenn.hpp>
#include <cblas.h>

namespace ssnake {

    static CBLAS_ORDER ord = CblasRowMajor;

    std::array<double, OV_SIZE> SnakeNN::propagate(std::array<double, IV_SIZE> ip)
    const {

      assert(weights.size() >= hyper_size(hpv));

      std::array<double, OV_SIZE> result = {0};
      if(hpv.empty())
      {
        assert(!weights.empty());
        for(unsigned i = 0; i < OV_SIZE; i++)
        {
          result[i] = weights[OV_SIZE * IV_SIZE + i];
        }
        cblas_dgemv(
          ord, CblasNoTrans, OV_SIZE, IV_SIZE, 1,
          weights.data(), OV_SIZE, ip.data(), 1, 1, result.data(), 1
        );
        return result;
      }

      unsigned s = 4;
      unsigned shift = 0, dshift = 0, tshift;
      for(auto h: hpv) if(h > s) s = h;
      double* intermediate =
        static_cast<double*>(malloc(sizeof(double) * s));
      const double* w = weights.data();
      for(unsigned i = 0; i < hpv[0]; i++)
      {
        intermediate[i] = w[IV_SIZE*hpv[0] + i];
      }
      cblas_dgemv(
        ord, CblasNoTrans, hpv[0], IV_SIZE, 1,
        w, IV_SIZE, ip.data(), 1, 1, intermediate, 1
      );
      shift = s;
      w += (IV_SIZE + 1) * hpv[0];
      for(unsigned i = 0; i < hpv.size() - 1; i++)
      {
        for(unsigned j = 0; j < hpv[i + 1]; j++)
        {
          intermediate[i + shift] = w[hpv[i]*hpv[i+1] + j];
        }
        cblas_dgemv(
          ord, CblasNoTrans, hpv[i + 1], hpv[i], 1,
          w, hpv[i], intermediate + dshift, 1, 1, intermediate + shift, 1
        );
        tshift = shift;
        shift = dshift;
        dshift = tshift;
        w += (1 + hpv[i]) * hpv[i + 1];
      }
      for(unsigned i = 0; i < OV_SIZE; i++)
      {
        result[i] = w[hpv.back() * OV_SIZE + i];
      }
      cblas_dgemv(
        ord, CblasNoTrans, OV_SIZE, hpv.back(), 1,
        w, hpv.back(), intermediate + dshift, 1, 1, result.data(), 1
      );
      free(intermediate);
      return result;
    }

    std::array<double, OV_SIZE> SnakeNN::propagate(const GameBoard& g) const {
      assert(IV_SIZE >= 64);
      auto df = g.distance_to_food();
      auto dW = g.distance_to_border();
      auto dS = g.distance_to_obstacle();
      auto hunger = g.hunger();

      double dfmod = sqrt(df[0]*df[0] + df[1]*df[1]);
      double dfx = df[0]/dfmod, dfy = df[1]/dfmod;

      std::array<double, IV_SIZE> result = {hunger, dfx, dfy, dfmod};
      for(unsigned i = 0; i < 8; i++)
      {
        result[i + 3] = dW[i];
        result[i + 11] = dS[i];
      }
      for(unsigned i = 0; i < 5; i++)
      {
        for(unsigned j = 0; j < 4; j++)
        {
          result[20 + 4*i + j] = (memory[i] == j);
        }
      }
      result[40] = static_cast<double>(rand() % 1000000)/1000000;
      result[41] = df[0]; result[42] = df[1];
      result[43] = g.nearest_obstacle();
      result[44] = (g.height() + g.width())/2;
      result[45] = g.size()/g.height();
      result[46] = g.size()/g.width();
      result[47] = g.height()/g.width();
      result[48] = g.height();
      result[49] = g.width();
      result[50] = g.head_pos()[0]; result[51] = g.head_pos()[1];
      result[52] = static_cast<double>(g.max_hunger())/1000;
      result[53] = g.size();
      return propagate(result);
    }

    Direction SnakeNN::move(const GameBoard& g) {
      auto ov = propagate(g);
      //printf("{%lf, %lf, %lf, %lf}", ov[0], ov[1], ov[2], ov[3]);
      auto mv = static_cast<Direction>(0);
      auto sc = ov[0];
      for(unsigned i = 1; i < OV_SIZE; i++)
      {
        if(ov[i] > sc)
        {
          sc = ov[i];
          mv = static_cast<Direction>(i);
        }
      }
      memmove(memory.data(), memory.data() + 1, 4 * sizeof(Direction));
      memory[4] = mv;
      return mv;
    }
}
