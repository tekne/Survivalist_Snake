#include <game_interface.hpp>
#include <game.hpp>
#include <ncurses.h>

namespace ssnake {

  const GameBoard& printBoard(const GameBoard& g)
  {
    move(0, 0);
    for(long j = 0; j <= g.width(); j++)
    {
      addch('X');
    }
    printw("X\n");
    for(long i = 0; i < g.height(); i++)
    {
      addch('X');
      for(long j = 0; j < g.width(); j++)
      {
        if(g.is_head({j, i}))
        {
          addch('H' | A_BOLD);
        }
        else if(g.is_food({j, i}))
        {
          addch('F' | A_BOLD);
        }
        else if(g.is_snake({j, i}))
        {
          addch('#');
        }
        else
        {
          addch(' ');
        }
      }
      printw("X\n");
    }
    for(long j = 0; j <= g.width(); j++)
    {
      addch('X');
    }
    printw("X\n");
    printw(
      "HX: %ld, HY: %ld, MEAL: %zu, TURN: %zu --> HUNGER: %lf, SCORE: %lf, DF = \
{%ld, %ld}                                                                   \n\
DW = {%lf, %lf, %lf, %lf, %lf, %lf, %lf, %lf}                                \n\
DS = {%lf, %lf, %lf, %lf, %lf, %lf, %lf, %lf}                                 ",
      g.head_pos().x,
      g.head_pos().y,
      g.prev_meal(),
      g.turns(),
      g.hunger(),
      g.score(),
      g.distance_to_food()[0],
      g.distance_to_food()[1],
      g.distance_to_border()[0],
      g.distance_to_border()[1],
      g.distance_to_border()[2],
      g.distance_to_border()[3],
      g.distance_to_border()[4],
      g.distance_to_border()[5],
      g.distance_to_border()[6],
      g.distance_to_border()[7],
      g.distance_to_obstacle()[0],
      g.distance_to_obstacle()[1],
      g.distance_to_obstacle()[2],
      g.distance_to_obstacle()[3],
      g.distance_to_obstacle()[4],
      g.distance_to_obstacle()[5],
      g.distance_to_obstacle()[6],
      g.distance_to_obstacle()[7]
    );
    refresh();
    return g;
  }

  void driveBoard(GameBoard& g, char command)
  {
    if((command == 'w') || (command == 'W'))
    {
      g.move_snake(DIR_S);
    }
    else if((command == 's') || (command == 'S'))
    {
      g.move_snake(DIR_N);
    }
    else if((command == 'a') || (command == 'A'))
    {
      g.move_snake(DIR_W);
    }
    else if((command == 'd') || (command == 'D'))
    {
      g.move_snake(DIR_E);
    }
  }

  void initializeScreen()
  {
    initscr();
    endwin();
  }

  void deleteScreen()
  {
    delwin(stdscr);
  }

  void endWindow()
  {
    endwin();
  }

  void userPlay(GameBoard& g)
  {
    refresh();
    cbreak();
    noecho();
    printBoard(g);
    while(!g.is_over()){
      char c = getch();
      if(c == 'X') break;
      driveBoard(g, c);
      printBoard(g);
    }
    endwin();
  }

}
