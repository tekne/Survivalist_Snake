#include <snakenn.hpp>

namespace ssnake {

  size_t hyper_size(const unsigned* hyper, unsigned c)
  {
    if(!hyper || !c) return (IV_SIZE + 1) * OV_SIZE;

    size_t result = (IV_SIZE + 1) * hyper[0];
    for(unsigned i = 0; i < c - 1; i++)
    {
      result += (hyper[i] + 1) * hyper[i + 1];
    }
    result += (hyper[c - 1] + 1) * OV_SIZE;
    return result;
  }

}
