#include <train.hpp>
#include <manager.hpp>
#include <snakenn.hpp>

#include <random>
#include <cstdio>
#include <cstdlib>
#include <fstream>

int main(int argc, char** argv)
{
  if(argc < 2)
  {
    fprintf(stderr, "Must have at least 1 argument!\n");
    return 1;
  }
  if(argc > 10)
  {
    fprintf(stderr, "Must have at most 9 arguments, got %i\n", argc - 1);
    return 1;
  }
  long X, Y;
  long gens;
  double rad_l;
  long pop;
  long nt;
  long rep;
  if(argc > 3)
  {
    if(!(X = strtol(argv[3], NULL, 0)))
    {
      fprintf(stderr, "Invalid integer %s passed as 3rd argument\n", argv[3]);
      return 1;
    }
    if(argc > 4)
    {
        if(!(Y = strtol(argv[4], NULL, 0)))
        {
          fprintf(stderr, "Invalid integer %s passed as 4th argument\n", argv[4]);
          return 1;
        }
    }
    else X = Y;
  }
  else
  {
    printf("Input board dimensions (X, Y): ");
    if(scanf(" ( %ld , %ld )", &X, &Y) != 2)
    {
      fprintf(stderr, "Matching error!\n");
      return 1;
    };
    if((X <= 0) || (Y <= 0))
    {
      fprintf(stderr, "Invalid board dimensions (%ld, %ld)\n", X, Y);
    }
  }
  if(argc > 5)
  {
    if((gens = strtol(argv[5], NULL, 0)) <= 0)
    {
      fprintf(stderr, "Invalid number of generations %s\n", argv[5]);
    }
  }
  else
  {
    printf("Input number of generations: ");
    scanf(" %ld", &gens);
  }
  if(argc > 6)
  {
    if(!(rad_l = strtod(argv[6], NULL)))
    {
      fprintf(stderr, "Invalid radiation level %s\n", argv[6]);
      return 1;
    }
  }
  else
  {
    printf("Input radiation level: ");
    scanf(" %lf", &rad_l);
  }
  if(argc > 7)
  {
    if((pop = strtol(argv[7], NULL, 0)) <= 0)
    {
      fprintf(stderr, "Invalid population %s\n", argv[7]);
      return 1;
    }
  }
  else
  {
    printf("Input desired population: ");
    scanf(" %ld", &pop);
    if(pop <= 0)
    {
      fprintf(stderr, "Invalid population!\n");
      return 1;
    }
  }
  if(argc > 8)
  {
    if((rep = strtol(argv[8], NULL, 0)) <= 0)
    {
      fprintf(stderr, "Invalid number of repetitions %s\n", argv[8]);
    }
  }
  else
  {
    printf("Input number of repetitions: ");
    scanf(" %ld", &rep);
    if(rep <= 0)
    {
      fprintf(stderr, "Invalid number of repetitions!\n");
      return 1;
    }
  }
  if(argc > 9)
  {
    if((nt = strtol(argv[9], NULL, 0)) <= 0)
    {
      fprintf(stderr, "Invalid number of threads %s\n", argv[8]);
    }
  }
  else
  {
    printf("Input number of threads: ");
    scanf(" %ld", &nt);
    if(nt <= 0)
    {
      fprintf(stderr, "Invalid number of threads!\n");
      return 1;
    }
  }

  std::normal_distribution<> d = std::normal_distribution<>(0, rad_l);
  std::default_random_engine g = std::default_random_engine();

  fprintf(stderr, "Loading snakes...\n");

  std::fstream file(argv[1]);
  std::vector<ssnake::SnakeNN> sv = ssnake::load_snakefile(file);

  fprintf(stderr, "Loaded %zu snakes\n", sv.size());

  fprintf(stderr, "Preparing to clone generation...\n");
  ssnake::clone_generation(sv, sv.size(), (pop + sv.size())/sv.size(), d, g);
  fprintf(stderr, "Cloned generation\n");

  std::vector<ssnake::GameBoard> gv;
  gv.emplace_back(X, Y);

  for(long i = 0; i < gens; i++)
  {
    fprintf(stderr, "Training Generation #%li...\n", i);
    ssnake::score_generation(sv, gv, rep, nt);
    auto stat = score_statistics(sv);
    fprintf(
      stderr,
      "Average Score: %lf, (σ = %lf), "
      "Average Max Score: %lf (σ = %lf), "
      "Average Min Score: %lf (σ = %lf), "
      "Max Score: %lf, Min Score: %lf\n",
      stat[0],
      stat[1],
      stat[2],
      stat[3],
      stat[4],
      stat[5],
      stat[6],
      stat[7]
    );
    ssnake::stratify_generation(sv, ((sv.size() < 5)?sv.size():5));
    if(sv.size() >= 5)
    fprintf(stderr, "Top 5 scores: %lf, %lf, %lf, %lf, %lf\n",
      sv[0].score(),
      sv[1].score(),
      sv[2].score(),
      sv[3].score(),
      sv[4].score()
    );
    ssnake::clone_generation(sv, 5, (sv.size() + 4)/5, d, g);
  }

  file.close();
  if(argc > 2)
  {
    fprintf(stderr, "Opening output file %s\n", argv[2]);
    file.open(argv[2], std::fstream::out);
  }
  else
  {
    fprintf(stderr, "Opening output file %s\n", argv[1]);
    file.open(argv[1], std::fstream::out);
  }
  ssnake::store_snakefile(file, sv);
  return 0;
}
